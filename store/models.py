# coding: utf-8
from django.db import models
from django.contrib.auth.models import AbstractUser
import json

# Usuário
class User(AbstractUser):
    qq = models.CharField(max_length=20, blank=True, null=True, verbose_name='Número QQ')
    mobile = models.CharField(max_length=11, blank=True, null=True, unique=True, verbose_name='Número de celular')

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = verbose_name
        ordering = ['-id']

    def __str__(self):
        return self.username

# Anúncio
class Ad(models.Model):
    title = models.CharField(max_length=50, verbose_name='Título')
    image_url = models.ImageField(upload_to='ad/%Y/%m', verbose_name='Caminho da imagem')
    date_publish = models.DateTimeField(auto_now_add=True, verbose_name='Data de publicação')
    index = models.IntegerField(default=1, verbose_name='Ordem de exibição')

    class Meta:
        verbose_name = 'Anúncio'
        verbose_name_plural = verbose_name
        ordering = ['index', 'id']

    def __str__(self):
        return self.title

# Categoria
class Category(models.Model):
    typ = models.CharField(max_length=20, verbose_name='Categoria principal')
    name = models.CharField(max_length=30, verbose_name='Nome da categoria')
    index = models.IntegerField(default=1, verbose_name='Ordem da categoria')
    # 0 representa masculino, 1 representa feminino
    sex = models.IntegerField(default=0, verbose_name='Sexo')

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = verbose_name
        ordering = ['index', 'id']

    def __str__(self):
        str = "Masculino" if self.sex == 0 else "Feminino"
        return self.name + "---" + str

# Marca
class Brand(models.Model):
    name = models.CharField(max_length=30, verbose_name='Nome da marca')
    index = models.IntegerField(default=1, verbose_name='Ordem de exibição')

    class Meta:
        verbose_name = 'Marca'
        verbose_name_plural = verbose_name
        ordering = ['index',]

    def __str__(self):
        return self.name

# Tamanho
class Size(models.Model):
    name = models.CharField(max_length=20, verbose_name='Tamanho')
    index = models.IntegerField(default=1, verbose_name='Ordem de exibição')

    class Meta:
        verbose_name = 'Tamanho'
        verbose_name_plural = verbose_name
        ordering = ['index',]

    def __str__(self):
        return self.name

# Etiqueta
class Tag(models.Model):
    name = models.CharField(max_length=30, verbose_name='Etiqueta')

    class Meta:
        verbose_name = 'Etiqueta'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

# Produtos incluindo roupas, sapatos, etc.
class Clothing(models.Model):
    category = models.ForeignKey(Category, verbose_name='Categoria')
    name = models.CharField(max_length=30, verbose_name='Nome')
    brand = models.ForeignKey(Brand, verbose_name='Marca')
    size = models.ManyToManyField(Size, verbose_name='Tamanho')
    old_price = models.FloatField(default=0.0, verbose_name='Preço antigo')
    new_price = models.FloatField(default=0.0, verbose_name='Preço atual')
    discount = models.FloatField(default=1, verbose_name='Desconto')
    desc = models.CharField(max_length=100, verbose_name='Descrição')
    sales = models.IntegerField(default=0, verbose_name='Vendas')
    tag = models.ManyToManyField(Tag, verbose_name='Etiqueta')
    num = models.IntegerField(default=0, verbose_name='Estoque')
    image_url_i = models.ImageField(upload_to='clothing/%Y/%m', default='clothing/default.jpg', verbose_name='Caminho da imagem exibição')
    image_url_l = models.ImageField(upload_to='clothing/%Y/%m', default='clothing/default.jpg', verbose_name='Caminho da imagem detalhe 1')
    image_url_m = models.ImageField(upload_to='clothing/%Y/%m', default='clothing/default.jpg', verbose_name='Caminho da imagem detalhe 2')
    image_url_r = models.ImageField(upload_to='clothing/%Y/%m', default='clothing/default.jpg', verbose_name='Caminho da imagem detalhe 3')
    image_url_c = models.ImageField(upload_to='clothing/%Y/%m', default='clothing/ce.jpg', verbose_name='Caminho da imagem carrinho')

    class Meta:
        verbose_name = 'Produto'
        verbose_name_plural = verbose_name
        ordering = ['id']

    def __str__(self):
        return self.brand.name + "---" + self.category.name

# Item do carrinho de compras
class Caritem(models.Model):
    clothing = models.ForeignKey(Clothing, verbose_name='Item do produto no carrinho')
    quantity = models.IntegerField(default=0, verbose_name='Quantidade')
    sum_price = models.FloatField(default=0.0, verbose_name='Total')

    class Meta:
        verbose_name = 'Item do carrinho'
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.id)

# Carrinho de compras
class Cart(object):
    def __init__(self):
        self.items = []
        self.total_price = 0.0

    def add(self, clothing):
        self.total_price += clothing.new_price
        for item in self.items:
            if item.clothing.id == clothing.id:
                item.quantity += 1
                item.sum_price += clothing.new_price
                return
        else:
            self.items.append(Caritem(clothing=clothing, quantity=1, sum_price=clothing.new_price))
